﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RunTimeApps.Caching.Interfaces;
using StackExchange.Redis;
using RunTimeApps.Common.Utils;

namespace RunTimeApps.Caching.Redis
{
    public class RedisCacheService : ICacheService
    {
        private readonly IDatabase _dataBase = null;
        private readonly ISerializer _serializer = null;
        private readonly ILogger _logger = null;
        private readonly RedisConfiguration _config = null;
        public RedisCacheService(RedisConfiguration configuration, ISerializer serializer
                               , IConnectionFactory connectionFactory = null, ILogger logger = null
                               , int db = 0)
        {
            _config = configuration;
            _logger = logger;
            IConnectionFactory _connectionFactory = connectionFactory == null ? new RedisConnectionFactory() : connectionFactory;
            _dataBase = Task.Run(async () => await _connectionFactory.GetDataBase(configuration, db)).Result;
        }

        public void Delete(string key)
        {
            _dataBase.KeyDelete(key);
        }

        public async Task DeleteAsync(string key)
        {
            await _dataBase.KeyDeleteAsync(key);
        }

        public string GetString(string key, string region = null)
        {
            var value =  _dataBase.StringGet(key);
            return value.HasValue ? (string)value : null;
        }

        public async Task<string> GetStringAsync(string key, string region = null)
        {
            var value = await _dataBase.StringGetAsync(key);
            return value.HasValue ? (string)value : null;
        }

        public string[] GetStrings(string[] keys, string region = null)
        {
            var length = keys.Length;
            var redisKeys = new RedisKey[length];
            for (var i = 0; i < length; i++)
                redisKeys[i] = keys[i];
            var redisValues = _dataBase.StringGet(redisKeys);
            var values = new string[length];
            for (var i = 0; i < length; i++)
                values[i] = redisValues[i];
            return values;
        }

        public async Task<string[]> GetStringsAsync(string[] keys, string region = null)
        {
            var length = keys.Length;
            var redisKeys = new RedisKey[length];
            for (var i = 0; i < length; i++)
                redisKeys[i] = keys[i];
            var redisValues = await _dataBase.StringGetAsync(redisKeys);
            var values = new string[length];
            for (var i = 0; i < length; i++)
                values[i] = redisValues[i];
            return values;
        }

        public T GetValue<T>(string key, string region = null)
        {
            var value = _dataBase.StringGet(key);
            return Deserialize<T>(value);
        }

        public async Task<T> GetValueAsync<T>(string key, string region = null)
        {
            var value = await _dataBase.StringGetAsync(key);
            return Deserialize<T>(value);
        }

        public IDictionary<string, T> GetValues<T>(string[] keys)
        {
            int length = keys.Length;
            RedisKey[] redisKeys = new RedisKey[length];
            for (int i = 0; i < length; i++)
                redisKeys[i] = keys[i];
            RedisValue[] redisValues = _dataBase.StringGet(redisKeys);
            IDictionary<string, T> result = new Dictionary<string, T>(length);
            for (int i = 0; i < length; i++)
                result.Add(keys[i], Deserialize<T>(redisValues[i]));
            return result;
        }

        public async Task<IDictionary<string, T>> GetValuesAsync<T>(string[] keys)
        {
            int length = keys.Length;
            RedisKey[] redisKeys = new RedisKey[length];
            for (int i = 0; i < length; i++)
                redisKeys[i] = keys[i];
            RedisValue[] redisValues = await _dataBase.StringGetAsync(redisKeys);
            IDictionary<string, T> result = new Dictionary<string, T>(length);
            for (int i = 0; i < length; i++)
                result.Add(keys[i], Deserialize<T>(redisValues[i]));
            return result;
        }

        public void SetString(string key, string value, TimeSpan lifeTime)
        {
            _dataBase.StringSet(key, value, lifeTime);
        }

        public async Task SetStringAsync(string key, string value, TimeSpan lifeTime)
        {
            await _dataBase.StringSetAsync(key, value, lifeTime);
        }

        public void SetValue(string key, object value, TimeSpan lifeTime)
        {
            var json = _serializer.Serialize(value);
            _dataBase.StringSet(key, json, lifeTime);
        }

        public async Task SetValueAsync(string key, object value, TimeSpan lifeTime)
        {
            var json = _serializer.Serialize(value);
            await _dataBase.StringSetAsync(key, json, lifeTime);
        }

        public void SetValue(string key, object value, DateTimeOffset expireTime)
        {
            var json = _serializer.Serialize(value);
            _dataBase.StringSet(key, json, expireTime - DateTimeOffset.UtcNow);
        }

        public async Task SetValueAsync(string key, object value, DateTimeOffset expireTime)
        {
            var json = _serializer.Serialize(value);
            await _dataBase.StringSetAsync(key, json, expireTime - DateTimeOffset.UtcNow);
        }

        #region Private Method
        private T Deserialize<T>(string value)
        {
            if (string.IsNullOrEmpty(value))
                return default(T);
            try
            {
                return _serializer.Deserialize<T>(value);
            }
            catch (Exception e)
            {
                if (_logger != null)
                    _logger.Log();
                return default(T);
            }
        }
        #endregion

        #region IDisposable Members
        private bool _disposed = false;
        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                //Dispose Managed Resources
            }

            //Dispose Unmanaged Resources

            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
        }

        ~RedisCacheService()
        {
            Dispose(false);
        }
        #endregion
    }
}
