﻿using StackExchange.Redis;
using System.Threading.Tasks;

namespace RunTimeApps.Caching.Redis
{
    public interface IConnectionFactory
    {
        Task<IDatabase> GetDataBase(RedisConfiguration configuration, int db);
        Task<ConnectionMultiplexer> GetConnection(RedisConfiguration configuration);
    }
}