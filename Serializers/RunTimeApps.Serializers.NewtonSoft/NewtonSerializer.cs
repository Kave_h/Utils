﻿using RunTimeApps.Common.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace RunTimeApps.Serializers.NewtonSoft
{
    public class NewtonSerializer : ISerializer
    {
        public NewtonSerializer()
        {
            JsonConvert.DefaultSettings = SerializerOptions.Global;
        }

        public T Deserialize<T>(string json, object options = null)
        {
            var newtonOption = options as JsonSerializerSettings;
            return JsonConvert.DeserializeObject<T>(json, newtonOption);
        }

        public T Merge<T>(IEnumerable<string> jsons) where T : class
        {
            if (!jsons.Any())
                return null;
            JsonMergeSettings jsonMergeSettings = new JsonMergeSettings() { MergeArrayHandling = MergeArrayHandling.Union, MergeNullValueHandling = MergeNullValueHandling.Ignore };
            JObject master = new JObject();
            JObject secObject = null;
            foreach (var json in jsons)
            {
                secObject = JObject.Parse(json);
                master.Merge(secObject, jsonMergeSettings);
            }
            return master.ToObject<T>();
        }

        public string Serialize<T>(T model, object options = null)
        {
            var newtonOption = options as JsonSerializerSettings;
            //todo do not merge this with master asshole 
            var setting = new JsonSerializerSettings();
            setting.NullValueHandling = NullValueHandling.Include;
            return JsonConvert.SerializeObject(model, setting);
        }

        public void SetDefaultConfiguration(object configuration)
        {
            throw new System.NotImplementedException();
        }
    }
}