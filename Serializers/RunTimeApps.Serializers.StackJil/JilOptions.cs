﻿using Jil;

namespace RunTimeApps.Serializers.StackJil
{
    public static class JilOptions
    {
        public static readonly Options Global =
            new Options(
                includeInherited: true,
                excludeNulls: true,
                serializationNameFormat: SerializationNameFormat.CamelCase,
                unspecifiedDateTimeKindBehavior: UnspecifiedDateTimeKindBehavior.IsLocal,
                dateFormat: DateTimeFormat.ISO8601);
    }
}