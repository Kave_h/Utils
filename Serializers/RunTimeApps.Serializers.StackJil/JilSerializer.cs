﻿using System.Collections.Generic;
using RunTimeApps.Common.Utils;
using Jil;

namespace RunTimeApps.Serializers.StackJil
{
    public class JilSerializer : ISerializer
    {
        public JilSerializer()
        {
            JSON.SetDefaultOptions(JilOptions.Global);
        }

        public T Deserialize<T>(string json, object options = null)
        {
            return JSON.Deserialize<T>(text: json, options: (Options)options);
        }

        public T Merge<T>(IEnumerable<string> jsons) where T : class
        {
            throw new System.NotImplementedException();
        }

        public string Serialize<T>(T model, object options = null)
        {
            return JSON.Serialize<T>(model, (Options)options);
        }

        public void SetDefaultConfiguration(object configuration)
        {
            throw new System.NotImplementedException();
        }
    }
}