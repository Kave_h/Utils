﻿using System.Collections.Generic;

namespace RunTimeApps.Common.Utils
{
    public interface ISerializer
    {
        string Serialize<T>(T model, object options = null);
        T Deserialize<T>(string json, object options = null);
        T Merge<T>(IEnumerable<string> jsons) where T : class;
        void SetDefaultConfiguration(object configuration);
    }
}