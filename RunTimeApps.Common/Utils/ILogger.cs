﻿using System;
using System.Threading.Tasks;

namespace RunTimeApps.Common.Utils
{
    public interface ILogger : IDisposable
    {
        void Log();
        Task LogAsync();
    }
}